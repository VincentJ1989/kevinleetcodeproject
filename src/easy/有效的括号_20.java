package easy;

import java.util.HashMap;
import java.util.Stack;

/**
 * 给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。
 * <p>
 * 有效字符串需满足：
 * <p>
 * 左括号必须用相同类型的右括号闭合。
 * 左括号必须以正确的顺序闭合。
 * 注意空字符串可被认为是有效字符串。
 * <p>
 * 示例 1:
 * <p>
 * 输入: "()"
 * 输出: true
 * 示例 2:
 * <p>
 * 输入: "()[]{}"
 * 输出: true
 * 示例 3:
 * <p>
 * 输入: "(]"
 * 输出: false
 * 示例 4:
 * <p>
 * 输入: "([)]"
 * 输出: false
 * 示例 5:
 * <p>
 * 输入: "{[]}"
 * 输出: true
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/valid-parentheses
 */
public class 有效的括号_20 {
    private static HashMap mappings;

    public static void main(String[] args) {
        mappings = new HashMap<Character, Character>();
        mappings.put(')', '(');
        mappings.put('}', '{');
        mappings.put(']', '[');

        String str = "({})){}";
        System.out.println(isValid(str));
    }

    /**
     * 官方解：辅助栈
     */
    public static boolean isValid(String s) {
        // 特殊情况
        if (s == null || s.length() % 2 != 0) {
            return false;
        }

        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (mappings.containsKey(c)) {
                // 右括号

                // 获取栈顶第一个，如果不一样直接false
                char topElement = stack.empty() ? '#' : stack.pop();
                if (topElement != (char) mappings.get(c)) {
                    return false;
                }
            } else {
                // 左括号，直接入栈
                stack.push(c);
            }
        }
        // 最后判断左括号的栈是否有值
        return stack.isEmpty();
    }
}
